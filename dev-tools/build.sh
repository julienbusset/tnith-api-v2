echo "Prepare..."
rm -rf dist
mkdir -p dist/images && cp -R -v public/images dist
mkdir -p dist/resources && cp -R -v public/resources dist
mkdir -p dist/js/jquery && cp -R -v public/js/jquery dist/js
echo "Bundle and minify..."
npm install -g esbuild
esbuild public/js/game.js --bundle --minify --outfile=dist/js/game.js
esbuild public/js/loadtitle.js --bundle --minify --outfile=dist/js/loadtitle.js
esbuild public/publicapp.js --bundle --minify --outfile=dist/publicapp.js
esbuild public/sw.js --bundle --minify --outfile=dist/sw.js
esbuild public/stylesheets/game.css --minify --outfile=dist/stylesheets/game.css
cp -v public/index.html dist/index.html
echo "Add licence at first line of each file..."
sed -i '1i\/\* The Needle In The Haystack - TNITH v2 \| Software under MIT Licence, 2019-2022 \@ Julien BUSSET \*\/' dist/js/game.js
sed -i '1i\/\* The Needle In The Haystack - TNITH v2 \| Software under MIT Licence, 2019-2022 \@ Julien BUSSET \*\/' dist/js/loadtitle.js
sed -i '1i\/\* The Needle In The Haystack - TNITH v2 \| Software under MIT Licence, 2019-2022 \@ Julien BUSSET \*\/' dist/publicapp.js
sed -i '1i\/\* The Needle In The Haystack - TNITH v2 \| Software under MIT Licence, 2019-2022 \@ Julien BUSSET \*\/' dist/sw.js
sed -i '1i\/\* The Needle In The Haystack - TNITH v2 \| Software under MIT Licence, 2019-2022 \@ Julien BUSSET \*\/' dist/stylesheets/game.css
sed -i '1i <!-- The Needle In The Haystack - TNITH v2 \| Software under MIT Licence, 2019-2022 \@ Julien BUSSET -->' dist/index.html
echo "Done!"