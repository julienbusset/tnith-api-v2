/* Script just to load the right title image depending on the language */
let languageList = window.navigator.userLanguage || window.navigator.language || navigator.browserLanguage || navigator.systemLanguage || "en";

let titleImage = 'title.png';
if (languageList.indexOf("fr") >= 0) {
    titleImage = 'title_fr.png'
}

const mainDiv = document.getElementById("mainContainer");
const titleDiv = document.createElement('img');

titleDiv.classList.add('title');
titleDiv.src = 'images/' + titleImage;

mainDiv.prepend(titleDiv);