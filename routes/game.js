const express = require('express');
const gameRouter = express.Router();

/* GET main game screen. */
gameRouter.get('/', function(req, res) {
  res.send('index');
});

module.exports = gameRouter;