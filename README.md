# TNITH v2

Game available at tnith.com, open source, made for easy contribution of anyone!

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/tnith/tnith-api-v2)

Software under MIT Licence, 2019-2022 @ Julien BUSSET

## Start to contribute with Gitpod

Thank you for improving the game! You should first read the [wiki](https://gitlab.com/tnith/tnith-api-v2/-/wikis/How-to-contribute) which explains everything useful about the game. You can also ask your questions or share with the community on [the Discord server of the game](https://discord.gg/bmKAAdPm4z).

[![Join the Discord server](https://assets-global.website-files.com/6257adef93867e50d84d30e2/62595384e89d1d54d704ece7_3437c10597c1526c3dbd98c737c2bcae.svg)](https://discord.gg/bmKAAdPm4z)

Once the Gitpod workspace is opened, you can begin to contribute by creating your own git branch or checking out an existing one (depending on the feature you want to work on) except the main branch. If you are a beginner, you can do this by simply executing this in the terminal:

`npm run start-contribution`

To run (hot reload) the app and test it, simply execute this, and a browser will open the game running:

`npm run start:hot`
If you want to start it without hot reload, execute:

`npm run start`

Once your developments are ok, you can build (`npm run build`) and git push your code. To do it all easily, you can simply run this:

`npm run publish-contribution`

Then ask for a merge request, or simply inform Beuj on Discord :-)

## Ready to work on

Some features are already required, and you can work on it if you want. Check the list [in the issue section](https://gitlab.com/tnith/tnith-api-v2/-/issues).

